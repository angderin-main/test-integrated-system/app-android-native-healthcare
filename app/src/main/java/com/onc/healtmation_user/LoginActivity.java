package com.onc.healtmation_user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class LoginActivity extends AppCompatActivity {

    private static int serverPort = 9999;
    private static String serverIP = "192.168.0.109";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        new Thread(new Client()).start();

        ImageButton imageLogo = (ImageButton) findViewById(R.id.logo_image);
//        imageLogo.setBackground("dscscs");

        EditText username = (EditText) findViewById(R.id.login_usernameText);
        EditText password = (EditText) findViewById(R.id.login_passwordText);
        Button login = (Button) findViewById(R.id.login_button);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, MenuActivity.class);
                startActivity(i);
            }
        });

        Button forgotPass = (Button) findViewById(R.id.forgotPassword);
        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotEmailActivity.class));
            }
        });
    }

    public class Client implements Runnable {

        @Override
        public void run() {
            try {
                ClientSocketHandler clientSocketHandler = new ClientSocketHandler(serverPort, InetAddress.getByName(serverIP));
                clientSocketHandler.openConnection();
                clientSocketHandler.sendDataToServer("berhasil");
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }
}
