package com.onc.healtmation_user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class VerificationEmailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_email);

        String emailLabel = "";
        Bundle extra = getIntent().getExtras();
        emailLabel = extra.getString("EMAIL");
        TextView email = (TextView) findViewById(R.id.email_label);
        email.setText(emailLabel);

        Button recovery = (Button) findViewById(R.id.recovery_button);
        recovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(VerificationEmailActivity.this, UpdatePasswordActivity.class);
                startActivity(i);
            }
        });

    }
}
