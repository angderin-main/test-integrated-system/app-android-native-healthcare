package com.onc.healtmation_user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ForgotEmailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_email);

        final EditText forgotEmail = (EditText) findViewById(R.id.forgotEmail_text);
        Button nextVerification = (Button) findViewById(R.id.nextVerification_button);
        nextVerification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ForgotEmailActivity.this, VerificationEmailActivity.class);
                i.putExtra("EMAIL",forgotEmail.getText().toString());
                startActivity(i);
            }
        });
    }
}
