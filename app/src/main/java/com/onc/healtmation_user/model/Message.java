package com.onc.healtmation_user.model;

import java.io.Serializable;

/**
 * Created by wece on 08/11/17.
 */

public class Message implements Serializable {
    private static final long serialVersionUID = 1L;
    public String type;
    public String content;

    public Message(String type, String content){
        this.type = type;
        this.content = content;
    }

    @Override
    public String toString(){
        return this.type + "#" + this.content;
    }
}
