package com.onc.healtmation_user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        ImageButton listAlergi = (ImageButton) findViewById(R.id.row_atasKiri);
//        listAlergi.setBackground();
        listAlergi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this,ListAlergiActivity.class);
                startActivity(i);
            }
        });
        ImageButton listPenyakitBawaan = (ImageButton) findViewById(R.id.row_atasKanan);
//        penyakitBawaan.setBackground();
        listPenyakitBawaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this,ListPenyakitBawaanActivity.class);
                startActivity(i);
            }
        });
        ImageButton listObat = (ImageButton) findViewById(R.id.row_bawahKiri);
//        listObat.setBackground();
        listObat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this, ListObatActivity.class);
                startActivity(i);
            }
        });
        ImageButton listHealtRecord = (ImageButton) findViewById(R.id.row_bawahKanan);
//        listHealtRecord.setBackground();
        listHealtRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this, ListHealtRecordActivity.class);
                startActivity(i);
            }
        });
        ImageButton editAccount = (ImageButton) findViewById(R.id.row_center);
//        editAccount.setBackground();
        editAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this, EditAccountActivity.class);
                startActivity(i);
            }
        });
    }
}
