package com.onc.healtmation_user;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by wece on 09/11/17.
 */

public class ClientSocketHandler {
    private Socket socket;
    private int serverPort;
    private InetAddress serverAddr;

    public ClientSocketHandler(int serverPort, InetAddress serverAddr) {
        this.serverPort = serverPort;
        this.serverAddr = serverAddr;
    }

    public void openConnection(){
        try {
            socket = new Socket(serverAddr, serverPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void sendDataToServer(String data){
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
            out.println(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
